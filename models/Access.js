var mongoose = require('mongoose');

var AccessSchema = new mongoose.Schema({
	site_id: { type : String },
	status: { type : String },
	user_id : { type : String },
	dealerId:{type:Number},
	credentials : { type : Object}
}, {versionKey: false});

//UserSchema.plugin(autoIncrement.plugin,{ model: 'user', field: '_id', startAt: 1 });

mongoose.model('Access', AccessSchema ,'access');