
	var operations_get 		= require('./utils/operations-get')
	exports.init = function (server){

		server.route({
			method: 'GET',
			path : '/spaces_dmm/{dealerId}',
			handler : operations_get.getSpacesDmm
		})
	}
