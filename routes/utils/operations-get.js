	const mongoose 		= require('mongoose');
		mongoose.set('debug', true);
	const request_sync  = require('sync-request');
	const async 		= require('async')

	const Access 			= mongoose.model('Access')
	const mJsonBody = { email: 'adminUserM4axi@maxipublica.com',password	: '3419HL4DtC2r' }
	const mUrlPostAccessToken = 'https://www.seminuevos.com/ptx/oauth/access_token?grant_type=client_credentials&client_id=maxipublica&client_secret=MaxiPubli123&scope=publish,catalog'
	const mUrlGetPacks = 'https://www.seminuevos.com/ptx/api/secure/catalog/search-dealer-by-id/'
	exports.getSpacesDmm = function(request, reply){
		getAccess(request, reply)
	}

/*router.get('/car2', function (req, resp, next){
	Car.find({"status":"review"},{},'_id condition title status description condition attributes dealer vehicle createDate images published_sites.mlm published_sites.vam status', function (err, cars){

		if(err) { return next(err) }
	resp.json(cars);

	})
})*/

	function getAccess ( request, reply){
		console.log('esto es reply', reply)
		async.waterfall([
			function(callback){ //Sacamos access_token de seminuevos
				const res = request_sync('POST', mUrlPostAccessToken , {
				  json : '',
				  headers :{ 'Content-Type': 'application/json', 'requestContentType' :'application/json'}
				});
				callback(null, JSON.parse(res.getBody('utf8'))['access_token'])
			},
			function( access_token_dmm,callback ){ //Sacamos access_token de admin
				const res = request_sync('POST', 'http://api.maxipublica.com/oauth/' , {
				  json : mJsonBody,
				  headers :{ 'Content-Type': 'application/json', 'requestContentType' :'application/json'}
				});
				callback(null,access_token_dmm, JSON.parse(res.getBody('utf8'))['access_token'])
			},
			function(access_token_dmm,access_token,callback){

				console.log("delaer --->> ", request.params.dealerId);
				//console.log("delaer... request", request.params.dealerId);
				var conditionDealer= request.params.dealerId;

				if (conditionDealer==="all" || conditionDealer=== null) {
					Access.find({ site_id : 'dmm', status: 'active'},'credentials.dealer_id user_id dealerId', function(err, resp){

					if(err) callback( null, err)
						callback(null,access_token_dmm,access_token, resp)
					})
				}
				else{

					Access.find({ site_id : 'dmm', status: 'active', dealerId:conditionDealer},'credentials.dealer_id user_id dealerId', function(err, resp){

					if(err) callback( null, err)
						callback(null,access_token_dmm,access_token, resp)
					})
				}

			},
			function (access_token_dmm,access_token,respUser, callback){
				console.log("respUser---"+respUser)
				var mResult = {}
				var mResultArray = []
				respUser.forEach(function(e){

					console.log("e---"+e.user_id)
					console.log("dealerId---"+e.dealerId)

					var mPacks = {}
					var mPacksPack = []
					console.log('Url packs'+mUrlGetPacks+e.credentials.dealer_id+'?access_token='+access_token_dmm)

					const res = request_sync('GET', mUrlGetPacks+e.credentials.dealer_id+'?access_token='+access_token_dmm, {
					  
					  headers :{ 'Content-Type': 'application/json', 'requestContentType' :'application/json'}
					});

					var mDataPost = JSON.parse(res.getBody('utf8'))['data']

					//console.log("mDataPost---",mDataPost)

					if(JSON.parse(res.getBody('utf8'))['status'] === 500 || JSON.parse(res.getBody('utf8'))['status'] === 401 ){
						mPacksPack.push({ available_listings : 0, remaining_listings : 0, used_listings : 0, listing_type_id : '1_Simples'},
										{ available_listings : 0, remaining_listings : 0, used_listings : 0, listing_type_id : '2_Destacados'})
						mPacks['packs'] 				= mPacksPack
						mPacks['date_start'] 			= new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString()
						mPacks['date_expires'] 			= new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).toISOString()
						mPacks['used_listings'] 		= 0
						mPacks['remaining_listings']	= 0
						mPacks['total_listings'] 		= 0
						mPacks['portal_name']			= 'DeMotores y Seminuevos.com'
						mPacks['status'] 				= 'false'
					}else {
						mPacksPack.push({
							available_listings 	: mDataPost['max_publish_slots'],
							remaining_listings 	: mDataPost['max_publish_slots'] - mDataPost['number_published'],
							used_listings 		: mDataPost['number_published'],
							listing_type_id 	: '1_Simples'
						},
						{
							available_listings	: mDataPost['max_highlight_slots'],
							remaining_listings 	: mDataPost['available_highlight'],
							used_listings 		: mDataPost['number_highlighted'],
							listing_type_id 	: '2_Destacados'
						}
						)
						console.log("mPacksPack---", mPacksPack)
						mPacks['packs'] 				= mPacksPack
						mPacks['date_start'] 			= new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString()
						mPacks['date_expires']			= new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).toISOString()
						mPacks['used_listings'] 		= mDataPost['number_highlighted'] 	+ mDataPost['number_published']
						mPacks['remaining_listings']	= mDataPost['max_publish_slots'] 	- mDataPost['number_published'] + mDataPost['available_highlight']
						mPacks['total_listings'] 		= mDataPost['max_publish_slots'] 	+ mDataPost['max_highlight_slots']
						mPacks['portal_name'] 			= 'DeMotores y Seminuevos.com'
						mPacks['active'] 				= 'true'
					console.log("mPacks---", mPacks)

					}
					request.mData = {}
					request.mData = mPacks
					var mRuta = 'http://api.maxipublica.com/spaces/'+e.dealerId+'?id_site=dmm&access_token='+access_token
					console.log('La ruta de spaces ->'+mRuta)
					//console.log('Mi paquete ',mPacks)
					const resSpaces = request_sync('POST', mRuta , {
					  json : mPacks,
					  headers :{ 'Content-Type': 'application/json'}
					});
				//	mResultArray.push({id_user: JSON.parse( resSpaces.getBody('utf8'))})
					//mResultArray.push({dealerId:parseInt(e.dealerId)})
				})
				//console.log("mResultArray---"+mResultArray)
				//mResult = mResultArray
				callback(null, mResult)
			}
		], function (e, r){
			reply(request.mData)
		})
	}
